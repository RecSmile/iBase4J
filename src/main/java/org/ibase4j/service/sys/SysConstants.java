package org.ibase4j.service.sys;

public interface SysConstants {
	static final String ACCOUNT_IS_NULL = "帐号不能为空.";
	static final String PASSWORD_IS_NULL = "密码不能为空.";
	static final String USER_ID_IS_NULL = "帐号不能为空.";
	static final String USER_IS_NULL = "用户Id[%1$s]错误.";
}
